Create a virtual environment

```
$ virtualenv flask
$ . flask/bin/activate
```
Install the dependencies
```
$ pip install -r requirements.txt
```
Run the application:
```
$ python app.py
```