from gevent import monkey
monkey.patch_all()

import os
import time
from threading import Thread
from flask import Flask, render_template, session, request
from flask.ext.socketio import SocketIO, emit, join_room, leave_room, \
    close_room, disconnect

from flask.ext.sqlalchemy import SQLAlchemy

_here = os.path.dirname(os.path.abspath(__file__))
app = Flask(__name__)
app.debug = True

app.config['SECRET_KEY'] = '\x83\x16\xe0\xc3\x7f\xd8\x04\x19\xb8\x90j\x95\xb7\xa5&\xb7\xb1\x8f\x87\x8b\xc2\xf4]\xbe\xf8\x1d\xc2!\xf5}\xadi'

db_path = os.path.join(_here,'game.db')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///{}'.format(db_path)

db = SQLAlchemy(app)
socketio = SocketIO(app)
thread = None

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(120),unique=True)
    email = db.Column(db.String(120), unique=True)

    def __init__(self, username, email):
        self.username = username
        self.email = email

    def __repr__(self):
        return '<User {u}>'.format(u=self.username)

def create_db():
    if not os.path.exists(db_path):
        db.create_all()
def destroy_db():
    os.unlink(db_path)

@app.route('/')
def index():
    return render_template('index.html')

def main():
    create_db()
    socketio.run(
        app,
        host=os.getenv('IP','127.0.0.1'),
        port=int(os.getenv('PORT',5000)),
    )
    
if __name__=='__main__':
    main()

