/*
  Inspired by:
  FadeObjectInOut.cs
  Hayden Scott-Baron (Dock) - http://starfruitgames.com
  6 Dec 2012 
 
  This allows you to easily fade an object and its children. 
  If an object is already partially faded it will continue from there. 
  If you choose a different speed, it will use the new speed. 
 
  NOTE: Requires materials with a shader that allows transparency
  through color.
*/
 
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Shapes;
 
public class FadeObjectInOut : MonoBehaviour, INextShape
{
 
    // publically editable speed
    public float defaultFadeTime = 0.5f; 
    public bool disableAfterFadeOut = false;
 
    public bool fadeStarted = false;
    public bool fadeDone = false;

    private Material material;
 
    // store colours
    private Dictionary<Renderer, Color> colorTable;
 
    // check the alpha value of most opaque object
    float MaxAlpha() {
        float maxAlpha = 0.0f; 
        Renderer[] rendererObjects = GetComponentsInChildren<Renderer>(); 
        foreach (Renderer renderer in rendererObjects) {
            maxAlpha = Mathf.Max (maxAlpha, renderer.material.color.a);
        }
        return maxAlpha; 
    }

    public void RefreshColorCache() {
        Renderer[] rendererObjects = GetComponentsInChildren<Renderer>();
        RefreshColorCache(rendererObjects);
    }

    public void NextShape(int shapeIndex) {
        // colorTable = null;
    }

    public void RefreshColorCache(Renderer[] rendererObjects) {
        //create a cache of colors if necessary
        if (colorTable == null) {
            colorTable = new Dictionary<Renderer, Color>();
        }
 
        // store the original colours for all child objects
        foreach(Renderer renderer in rendererObjects) {
            if (!colorTable.ContainsKey(renderer)) {
                Color color = renderer.material.color;
                if (color.a != 1.0f) {
                    color.a = 1.0f;
                }
                colorTable.Add(renderer, color);
            }
        }

    }

    // fade sequence
    IEnumerator FadeSequence (float fadeTime)
    {
        // For external blocking behavior
        fadeStarted = true;
        fadeDone = false;
        
        // log fading direction, then precalculate fading speed as a multiplier
        bool fadingOut = (fadeTime < 0.0f);
        float fadeSpeed = 1.0f / fadeTime;
        Debug.Log("fadeSpeed: "+fadeSpeed);
 
        // grab all child objects
        Renderer[] rendererObjects = GetComponentsInChildren<Renderer>();

        RefreshColorCache(rendererObjects);
 
        // make all objects visible
        foreach(Renderer renderer in rendererObjects) {
            renderer.enabled = true;
        }
 
 
        // get current max alpha
        float alphaValue = MaxAlpha();  
 
 
        // iterate to change alpha value 
        while ( (alphaValue >= 0.0f && fadingOut) ||
                (alphaValue <= 1.0f && !fadingOut) )  {
            alphaValue += Time.deltaTime * fadeSpeed; 
 
            foreach (Renderer renderer in rendererObjects) {
                // Debug.Log("FADER: "+gameObject+" fading out: "+fadingOut+ " renderer: "+renderer);
                Color newColor = colorTable[renderer];
                newColor.a = Mathf.Min ( newColor.a, alphaValue ); 
                newColor.a = Mathf.Clamp (newColor.a, 0.0f, 1.0f);
                renderer.material.SetColor("_Color", newColor) ;
            }
 
            yield return null; 
        }
 
        // turn objects off after fading out
        if (fadingOut && disableAfterFadeOut) {
            foreach (Renderer renderer in rendererObjects) {
                renderer.enabled = false;
            }
        }
 
 
        // Debug.Log (gameObject+": fade sequence end : " + fadingOut); 

        // For external blocking behavior
        fadeDone = true;
        fadeStarted = false;
    }
 
 
    public void FadeIn () {
        FadeIn (defaultFadeTime); 
    }
 
    public void FadeOut () {
        FadeOut (defaultFadeTime); 		
    }
 
    public void FadeIn (float fadeTime) {
        StopAllCoroutines();
        Debug.Log("Fade in time: "+fadeTime);
        StartCoroutine("FadeSequence", fadeTime); 
    }
 
    public void FadeOut (float fadeTime) {
        StopAllCoroutines(); 
        Debug.Log("Fade out time: "+fadeTime);
        StartCoroutine("FadeSequence", -fadeTime); 
    }

}
