namespace Player {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using RoomPosition;
    using GameUtilities;
    using GameMessages;

    public class PlayerInput : MessageHandler {
        private Player.Positioner pos;

        private Message testShapeMatchMessage;
        private Message playerWadsMessage;
        
        public void Awake() {
            pos = gameObject.GetComponent<Player.Positioner>();

            testShapeMatchMessage = MessageUtils.New(
                MessageType.TestShapeMatch
            );

            playerWadsMessage = MessageUtils.New(
                MessageType.PlayerWADS
            );
        }

        public override void HandleMessage(Message message) {
            if (message.type == MessageType.ClickWall) {
                Position p = pos.position.transformToWall(message.w0);
                Debug.Log("NEW POSITION: "+p);
                pos.SetPosition(p);
            }
        }

        void SendTestShapeMatchMessage() {
            MessageBus.current.SendMessage(testShapeMatchMessage);
        }

        void SendPlayerWadsMessage() {
            MessageBus.current.SendMessage(playerWadsMessage);
        }

        bool horizontalSelected = false;
        bool verticalSelected = false;
        bool jumpSelected = false;

        void Update() {
            // ************************
            // User input
            // ************************

            if (Application.platform != RuntimePlatform.IPhonePlayer) {
                if (Input.GetAxisRaw("Horizontal")==-1) {
                    if (!horizontalSelected) {
                        pos.MoveLeft();
                        SendPlayerWadsMessage();
                        horizontalSelected = true;
                    }
                } else if (Input.GetAxisRaw("Horizontal")==1) {
                    if (!horizontalSelected) {
                        pos.MoveRight();
                        SendPlayerWadsMessage();
                        horizontalSelected = true;
                    }
                } else if (Input.GetAxisRaw("Horizontal")==0) {
                    horizontalSelected = false;
                }

                if (Input.GetAxisRaw("Vertical")==1) {
                    if (!verticalSelected) {
                        pos.MoveUp();
                        SendPlayerWadsMessage();
                        verticalSelected = true;
                    }
                } else if (Input.GetAxisRaw("Vertical")==-1) {
                    if (!verticalSelected) {
                        pos.MoveDown();
                        SendPlayerWadsMessage();
                        verticalSelected = true;
                    }
                } else if (Input.GetAxisRaw("Vertical")==0) {
                    verticalSelected = false;
                }

                // Check for Jump axis, where Jump (i.e. space)
                // "checks" for correct alignment

                if (Input.GetAxisRaw("Jump")==1) {
                    if (!jumpSelected) {
                        SendTestShapeMatchMessage();
                        jumpSelected = true;
                    }
                } else if (Input.GetAxisRaw("Jump")==0) {
                    jumpSelected = false;
                }
            }

        }
    }
}
