namespace Player {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using RoomPosition;
    using GameUtilities;
    using GameMessages;
    using Shapes;

    public class Positioner : RoomPositionerMonoBehaviour, INextShape {
        // current rotation wrt right wall (basis of calculation for
        // solution in GameUtilities.Utils
        public int currentRotation;
        
        private WallList walls = new WallList(
            (int) (Wall.Left | Wall.Floor | Wall.Right | Wall.Ceiling)
        );

        private Message positionMessage;
        
        void Awake() {
            positionMessage = new Message();
            positionMessage.type = MessageType.PlayerPosition;
        }

        void Start() {
            ResetRandomPosition();
            StartCoroutine(ClockwiseCr());
            StartCoroutine(ForwardCr());
        }

        public void NextShape(int shapeIndex) {
            ResetRandomPosition();
        }

        public void ResetRandomPosition() {
            // gameObject.SetActive(false);

            // Select random wall to place the player shape
            Wall w = walls.RandomWall();
            // Debug.Log("NEW WALL: "+w);

            // Select random position on that wall
            position = Position.RandomPosition(w);

            RefreshPosition();

            // gameObject.SetActive(true);
        }

        public override int ClampK(int value) {
            return value > 3 ? 3 : (value < 1 ? 1 : value);
        }

        public bool refreshPosition = false; 
        void RefreshPosition() {
            ResetRoomPosition();

            SendPositionMessage();
            refreshPosition = false;
        }

        public void SetPosition(Position newPosition) {
            position = newPosition;
            RefreshPosition();
        }

        void SendPositionMessage() {
            // Debug.Log(position.projection);
            positionMessage.go0 = gameObject;
            positionMessage.p0 = position;
            positionMessage.i0 = currentRotation;
            MessageBus.current.SendMessage(positionMessage);
        }

        public float horizontalAxis, verticalAxis;
        public float maxClockwiseSpeed=4f, maxForwardSpeed=2f;
        public float clockwiseSpeed, forwardSpeed;
        private IEnumerator clockwiseCr;
        private IEnumerator forwardCr;
        public bool movementStarted = false;


        void Update() {
            if (clockwiseSpeed!=0 && clockwiseCr==null) {
                clockwiseCr = ClockwiseCr();
                StartCoroutine(clockwiseCr);
            } else if ( clockwiseCr != null &&
                        (Input.GetKeyDown(KeyCode.RightArrow) ||
                         Input.GetKeyDown(KeyCode.LeftArrow)) ) {
                StopCoroutine(clockwiseCr);
                clockwiseCr = ClockwiseCr();
                StartCoroutine(clockwiseCr);
            }

            if (forwardSpeed!=0 && forwardCr==null) {
                forwardCr = ForwardCr();
                StartCoroutine(forwardCr);
            } else if ( forwardCr != null &&
                        (Input.GetKeyDown(KeyCode.UpArrow) ||
                         Input.GetKeyDown(KeyCode.DownArrow)) ) {
                StopCoroutine(forwardCr);
                forwardCr = ForwardCr();
                StartCoroutine(forwardCr);
            }

            if (refreshPosition) {
                RefreshPosition();
            }
        
        }

        IEnumerator ClockwiseCr() {
            while (clockwiseSpeed != 0) {
                // Debug.Log(clockwiseSpeed);
                if (clockwiseSpeed > 0) {
                    CounterClockwise();
                } else if (clockwiseSpeed < 0) {
                    Clockwise();
                }
                float wait = Mathf.Clamp(1/Mathf.Abs(clockwiseSpeed),0f,0.5f);
                yield return new WaitForSeconds(wait);
            }
            clockwiseCr = null;
        }

        IEnumerator ForwardCr() {
            while(forwardSpeed != 0) {
                if (forwardSpeed > 0) {
                    Forward();
                } else if (forwardSpeed < 0) {
                    Backward();
                }
                float wait = Mathf.Clamp(1/Mathf.Abs(forwardSpeed),0f,0.5f);
                yield return new WaitForSeconds(wait);
            }
            forwardCr = null;
        }

        // IEnumerator MovementCoroutine() {
        //     while(clockwiseSpeed!=0 || l
        // }

        // Corroutine that causes the shape to move either clockwise
        // or forward or both (negative and positive directions) at
        // some rate. Rate determined by GetAxis("Horizontal") and
        // GetAxis("Vertical") and implemented using Waits.
        // 
        // How to implement independent clockwise and forward movement
        // threads? Master routine starts subroutines
        
        // IEnumerator
        void CounterClockwise() {
            // Vector3 rot = new Vector3(0,0,90);
            switch(wall) {
                case Wall.Left:
                    j -= 1;
                    if (OnWall(Wall.Floor)) {
                        // transition to floor
                        i = 1;  // leave left wall
                    }
                    break;

                case Wall.Right:
                    j += 1;
                    if (OnWall(Wall.Ceiling)) {
                        // transition to ceiling
                        i = 3;  // leave right wall
                    }
                    break;

                case Wall.Floor:
                    i += 1;
                    if (OnWall(Wall.Right)) {
                        // transition to right wall
                        j = 1;  // leave floor
                    }
                    break;

                case Wall.Ceiling:
                    i -= 1;
                    if (OnWall(Wall.Left)) {
                        // transition to left wall
                        j = 3;  // leave ceiling
                    }
                    break;
            }
            refreshPosition = true;
        }

        void Clockwise() {
            // Vector3 rot = new Vector3(0,0,-90);
            switch(wall) {
                case Wall.Left:
                    j += 1;
                    if (OnWall(Wall.Ceiling)) {
                        // transition to ceiling
                        i = 1;  // leave left wall
                    }
                    break;

                case Wall.Right:
                    j -= 1;
                    if (OnWall(Wall.Floor)) {
                        // transition to floor
                        i = 3;  // leave right wall
                    }
                    break;

                case Wall.Floor:
                    i -= 1;
                    if (OnWall(Wall.Left)) {
                        // transition to left wall
                        j = 1;  // leave floor
                    }
                    break;

                case Wall.Ceiling:
                    i += 1;
                    if (OnWall(Wall.Right)) {
                        // transition to right wall
                        j = 3;  // leave ceiling
                    }
                    break;

            }
            refreshPosition = true;

        }

        void Forward() {
            k -= 1;
            refreshPosition = true;
        }

        void Backward() {
            k += 1;
            refreshPosition = true;
        }

        public void MoveLeft() {
            switch(wall) {
                case Wall.Left:
                    k = k - 1 > 0 ? k - 1 : k;
                    break;

                case Wall.Right:
                    k = k + 1 < 4 ? k + 1 : k;
                    break;

                case Wall.Floor:
                    i = i - 1 > 0 ? i-1: i;
                    break;

                case Wall.Ceiling:
                    i = i - 1 > 0 ? i-1: i;
                    break;
            }
            refreshPosition = true;

        }

        public void MoveRight() {
            switch(wall) {
                case Wall.Left:
                    k = k + 1 < 4 ? k + 1 : k;
                    break;

                case Wall.Right:
                    k = k - 1 > 0 ? k - 1 : k;
                    break;

                case Wall.Floor:
                    i = i + 1 < 4 ? i+1: i;
                    break;

                case Wall.Ceiling:
                    i = i + 1 < 4 ? i+1: i;
                    break;
            }
            refreshPosition = true;

        }
        
        public void MoveUp() {
            switch(wall) {
                case Wall.Left:
                    j = j + 1 < 4 ? j + 1 : j;
                    break;

                case Wall.Right:
                    j = j + 1 < 4 ? j + 1 : j;
                    break;

                case Wall.Floor:
                    k = k + 1 < 4 ? k + 1: k;
                    break;

                case Wall.Ceiling:
                    k = k - 1 > 0 ? k - 1: k;
                    break;
            }
            refreshPosition = true;

        }

        public void MoveDown() {
            switch(wall) {
                case Wall.Left:
                    j = j - 1 > 0 ? j - 1 : j;
                    break;

                case Wall.Right:
                    j = j - 1 > 0 ? j - 1 : j;
                    break;

                case Wall.Floor:
                    k = k - 1 > 0 ? k - 1: k;
                    break;

                case Wall.Ceiling:
                    k = k + 1 < 4 ? k + 1: k;
                    break;
            }
            refreshPosition = true;

        }
        
        
    }
}

