﻿// - Flip animation (coin/poker chip flipping from one position to the
//   next
// - Needs smoothing

namespace GameController {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using GameMessages;

    public static class Names {
        public static string objectName = "Game Controller";
    }

    [System.Serializable]
    public class WallObjects {
        // The back wall of unstoppable destruction. Fear it.
        public GameObject back;
        // The other walls... not as interesting.
        public GameObject right;
        public GameObject left;
        public GameObject ceiling;
        public GameObject floor;

        // shim versions of the walls for when the player is moving
        // from one level to the next
        public GameObject backShim;
        public GameObject rightShim;
        public GameObject leftShim;
        public GameObject ceilingShim;
        public GameObject floorShim;
    }

    [System.Serializable]
    public class Cameras {
        public GameObject main;
    }
    
    [System.Serializable]
    public class Lights {
        public GameObject pointCenter;
    }

    public static class GC {
        private static GameObject _current = null;
        public static GameObject current {
            get {
                _current = _current ?? GameObject.Find(Names.objectName);
                return _current;
            }
        }
    }

    public partial class Objects: MonoBehaviour {
        private static Objects _current;
        public static Objects current {
            get {
                if (!_current) {
                    _current = FindObjectOfType( typeof(Objects) )
                        as Objects;
                }
                return _current;
            }
        }
    }

    public partial class Master: MessageHandler {
        private static Master _current;
        public static Master current {
            get {
                if (!_current) {
                    _current = FindObjectOfType( typeof(Master) )
                        as Master;
                }
                return _current;
            }
        }

    }

}

