namespace GameUtilities {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;

    public enum Wall {
        None=0, Right=1, Floor=2, Left=4, Ceiling=8, Back=16, 
    };

    public enum Layers {
        Wall=8,
        ProjectedShape,
        PlayerShape,
        DoorPanel,
        HighlightedWall,
        BackWall,
        SelectedWall,
    };

    public enum Shapes {
        Star=0,
        Triangle=1,
        Square=2,
    }

    public class WallList {
        public List<Wall> wallList;

        public WallList(int walls) {
            wallList = new List<Wall>();
            foreach (Wall wall in System.Enum.GetValues(typeof(Wall))) {
                if (wall != Wall.None) {
                    int w = (int)wall;
                    if ((walls & w) == w) {
                        wallList.Add(wall);
                    }
                }
            }
        }

        public Wall RandomWall() {
            return wallList[Random.Range(0,wallList.Count)];
        }
    }

    public static class Utils {
        public static IEnumerable<FileInfo> GetFiles(string rootDir,
                                                     string searchPattern) {
            Queue<DirectoryInfo> queue = new Queue<DirectoryInfo>();
            queue.Enqueue(new DirectoryInfo(rootDir));

            while (queue.Count > 0) {
                DirectoryInfo dirInfo = queue.Dequeue();
                try {
                    foreach (DirectoryInfo subDir in dirInfo.GetDirectories()) {
                        queue.Enqueue(subDir);
                    }
                } catch(System.Exception ex) {
                    Debug.LogError(ex);
                }
                
                FileInfo[] files = null;
                try {
                    if (searchPattern != null) {
                        files = dirInfo.GetFiles(searchPattern);
                    } else {
                        files = dirInfo.GetFiles();
                    }
                } catch (System.Exception ex) {
                    Debug.LogError(ex);
                }

                if (files != null) {
                    foreach(FileInfo f in files) {
                        yield return f;
                    }
                }
            }
        }        
        
        public static IEnumerable<FileInfo> GetFiles(string rootDir) {
            foreach (FileInfo f in GetFiles(rootDir, null)) {
                yield return f;
            }
        }
    }

}
