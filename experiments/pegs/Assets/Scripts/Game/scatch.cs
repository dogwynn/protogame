        // public static Wall RandomWall() {
        //     System.Array values = System.Enum.GetValues(typeof(Wall));
        //     return (Wall)values.GetValue(Random.Range(1,values.Length));
        // }

        // public static Wall RandomWall(int walls) {
        //     return new WallList(walls).RandomWall();
        // }

        // public static Position RandomPosition(Wall wall) {
        //     Position pos = new Position();
        //     switch(wall) {
        //         case Wall.Left:
        //             pos.i = 0;
        //             pos.j = Random.Range(1,4);
        //             pos.k = Random.Range(1,4);
        //             break;
        //         case Wall.Right:
        //             pos.i = 4;
        //             pos.j = Random.Range(1,4);
        //             pos.k = Random.Range(1,4);
        //             break;
        //         case Wall.Floor:
        //             pos.j = 0;
        //             pos.i = Random.Range(1,4);
        //             pos.k = Random.Range(1,4);
        //             break;    
        //         case Wall.Ceiling:
        //             pos.j = 4;
        //             pos.i = Random.Range(1,4);
        //             pos.k = Random.Range(1,4);
        //             break;
        //         case Wall.Back:
        //             pos.k = 0;
        //             pos.i = Random.Range(1,4);
        //             pos.j = Random.Range(1,4);
        //             break;
        //     }
        //     pos.l = Random.Range(0,3);
        //     return pos;
        // }

        // public static int RandomRotationAmount() {
        //     return 90*Random.Range(0,3);
        // }

        // public static Vector3 Rotation(Wall wall, Position pos) {
        //     int amount = 0;
        //     if (pos.l == 1) {
        //         amount = 90;
        //     } else if (pos.l == 2) {
        //         amount = 180;
        //     } else if (pos.l == 3) {
        //         amount = 270;
        //     }
        //     return Rotation(wall, amount);
        // }

        // public static Vector3 Rotation(Wall wall, int amount) {
        //     int rotx = 0;
        //     int roty = 0;
        //     int rotz = 0;
        //     switch(wall) {
        //         case Wall.Left:
        //             rotx = -amount;
        //             break;
        //         case Wall.Right:
        //             rotx = amount;
        //             break;
        //         case Wall.Floor:
        //             roty = -amount;
        //             break;
        //         case Wall.Ceiling:
        //             roty = amount;
        //             break;
        //         case Wall.Back:
        //             rotz = amount;
        //             break;
        //     }
        //     return new Vector3(rotx, roty, rotz);
        // }

        // public static Position Back2Right(Position pos) {
        //     return new Position(4, pos.j, (4-pos.i));
        // }

        // public static Position Back2Left(Position pos) {
        //     return new Position(0, pos.j, pos.i);
        // }

        // public static Position Back2Floor(Position pos) {
        //     return new Position(pos.i, 0, pos.j);
        // }

        // public static Position Back2Ceiling(Position pos) {
        //     return new Position(pos.i, 4, (4-pos.j));
        // }

        // public static Position Solution(Position doorPosition, int doorRotation,
        //                                 int playerRotation) {
        //     // Given:
        //     //
        //     // doorPosition: ijk position for door 
        //     // 
        //     // doorRotation: current rotation of door wrt back wall
        //     // 
        //     // playerRotation: current rotation of the player shape
        //     //      wrt right wall
        //     // 
        //     // Return: solution ijk position for the player (i.e. have
        //     //         correct projected position and rotation on the
        //     //         back wall)
        //     // 
        //     System.Func<Position,Position> map = p => p;

        //     if(playerRotation == 0) {
        //         if(doorRotation == 0) {
        //             // right wall solution
        //             map = Back2Right; // solution = Back2Right(doorPosition);
        //         } else if(doorRotation == 270) {
        //             // floor solution
        //             map = Back2Floor; // solution = Back2Floor(doorPosition);
        //         } else if(doorRotation == 180) {
        //             // left wall solution
        //             map = Back2Left; // solution = Back2Left(doorPosition);
        //         } else {
        //             // ceiling solution
        //             map = Back2Ceiling; // solution = Back2Ceiling(doorPosition);
        //         }

        //     } else if (playerRotation == 90) {
        //         if(doorRotation == 0) {
        //             // ceiling solution
        //             map = Back2Ceiling; // solution = Back2Ceiling(doorPosition);
        //         } else if(doorRotation == 270) {
        //             // right wall solution
        //             map = Back2Right; // solution = Back2Right(doorPosition);
        //         } else if(doorRotation == 180) {
        //             // floor solution
        //             map = Back2Floor; // solution = Back2Floor(doorPosition);
        //         } else {
        //             // left wall solution
        //             map = Back2Left; // solution = Back2Left(doorPosition);
        //         }

        //     } else if (playerRotation == 180) {
        //         if(doorRotation == 0) {
        //             // left wall solution
        //             map = Back2Left; // solution = Back2Left(doorPosition);
        //         } else if(doorRotation == 270) {
        //             // ceiling solution
        //             map = Back2Ceiling; // solution = Back2Ceiling(doorPosition);
        //         } else if(doorRotation == 180) {
        //             // right wall solution
        //             map = Back2Right; // solution = Back2Right(doorPosition);
        //         } else {
        //             // floor solution
        //             map = Back2Floor; // solution = Back2Floor(doorPosition);
        //         }

        //     } else {
        //         if(doorRotation == 0) {
        //             // floor solution
        //             map = Back2Floor; // solution = Back2Floor(doorPosition);
        //         } else if(doorRotation == 270) {
        //             // left wall solution
        //             map = Back2Left; // solution = Back2Left(doorPosition);
        //         } else if(doorRotation == 180) {
        //             // ceiling solution
        //             map = Back2Ceiling; // solution = Back2Ceiling(doorPosition);
        //         } else {
        //             // right wall solution
        //             map = Back2Right; // solution = Back2Right(doorPosition);
        //         }
        //     }

        //     return map(doorPosition);
        // }

