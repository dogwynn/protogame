namespace GameUtilities {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;

    public class Position {
        // Data structure that encodes a posistion in the game area
        // ("room").
        // 
        // Characterized by i, j, k, l coordinates, where (i, j, k)
        // represent position and l represents rotation (0, 90, 180,
        // 270) with respect to the right wall.
        // 
        // To be used by other classes to project [i, j, k] to [x, y,
        // z] and rotation Eulers (e.g. the RoomPositionMonoBehaviour
        // class).
        public Position() {
        }

        public Position(int i, int j, int k, int l): this() {
            _i = i;
            _j = j;
            _k = k;
            _l = l;
        }

        public Position(int i, int j, int k): this(i,j,k,0) {
        }

        public Position(Position other): this(other.i, other.j, other.k, other.l) {}

        public static Position RandomPosition(Wall w) {
            Position pos = new Position();
            switch(w) {
                case Wall.Left:
                    pos.i = 0;
                    pos.j = Random.Range(1,4);
                    pos.k = Random.Range(1,4);
                    break;
                case Wall.Right:
                    pos.i = 4;
                    pos.j = Random.Range(1,4);
                    pos.k = Random.Range(1,4);
                    break;
                case Wall.Floor:
                    pos.j = 0;
                    pos.i = Random.Range(1,4);
                    pos.k = Random.Range(1,4);
                    break;    
                case Wall.Ceiling:
                    pos.j = 4;
                    pos.i = Random.Range(1,4);
                    pos.k = Random.Range(1,4);
                    break;
                case Wall.Back:
                    pos.k = 0;
                    pos.i = Random.Range(1,4);
                    pos.j = Random.Range(1,4);
                    break;
            }
            pos.l = Random.Range(0,3);
            return pos;
        }

        public override string ToString() {
            return System.String.Format(
                "( {0}, {1}, {2}, {3} ) on {4}", i, j, k, l, wall
            );
        }

        // ------------------------------------------------------------
        // Position coordinates (i, j, k)
        // 
        // (0, 0, 0) is the bottom left corner, farthest from the
        // camera. (4, 4, 4) is the bottom right corner, nearest to
        // the camera.
        // 
        // Coordinate i represents discrete positions wrt to the X
        // axis, where higher values of i are farther to the right on
        // the X axis.
        // 
        // If i==0, then the position is on the left wall.
        // If i==4, then the position is on the right wall.
        // 
        private int _i=0;
        public int i {
            get{ return _i; }
            set{ _i = value > 4 ? 4 : (value < 0 ? 0 : value); }
        }
        // 
        // Coordinate j represents discrete positions wrt to the Y
        // axis, where higher values of j are higher on the Y axis.
        // 
        // If j==0, then the position is on the floor.
        // If j==4, then the position is on the ceiling.
        // 
        private int _j=0;
        public int j {
            get{ return _j; }
            set{ _j = value > 4 ? 4 : (value < 0 ? 0 : value); }
        }
        // 
        // Coordinate k represents discrete positions wrt to the Z
        // axis, where higher values of k are closer to the camera.
        // 
        // If k==0, then the position is on the back wall.
        // 
        // k==4 is (currently) not defined, as there is no "front"
        // wall nearest the camera.
        // 
        private int _k=0;
        public int k {
            get{ return _k; }
            set{ _k = value > 4 ? 4 : (value < 0 ? 0 : value); }
        }

        // ------------------------------------------------------------
        // Rotatation coordinate l
        // 
        // l is an integer corresponding to a rotation (0 => 0, 1 =>
        // 90, 2 => 180, 3 => 270) with respect to the right wall.
        //
        private int _l=0;
        public int l {
            get{ return _l; }
            set{ _l = value > 3 ? 3 : (value < 0 ? 0 : value); }
        }

        public bool MatchIJK(Position pos) {
            return (pos.i == i && pos.j == j && pos.k == k);
        }
        public bool MatchIJKL(Position pos) {
            return (pos.i == i && pos.j == j && pos.k == k && pos.l == l);
        }

        public override bool Equals(System.Object obj) {
            // If parameter is null return false.
            if (obj == null) {
                return false;
            }

            // If parameter cannot be cast to Position return false.
            Position p = obj as Position;
            if ((System.Object)p == null) {
                return false;
            }

            // Return true if the fields match:
            return Equals(p);
        }
        
        public bool Equals(Position p) {
            if((object)p == null) {
                return false;
            }
            return MatchIJKL(p);
        }
        
        public static bool operator ==(Position a, Position b) {
            // If both are null, or both are same instance, return true.
            if (System.Object.ReferenceEquals(a, b)) {
                return true;
            }

            // If one is null, but not both, return false.
            if (((object)a == null) || ((object)b == null)) {
                return false;
            }

            // Return true if the fields match:
            return a.Equals(b);
        }

        public static bool operator !=(Position a, Position b) {
            return !(a==b);
        }
            
        public override int GetHashCode() {
            return i | j << 5 | k << 10 | l << 15;
        }

        public Wall wall {
            get {
                Wall w = Wall.None;
                if (OnWall(Wall.Left)) {
                    w = Wall.Left;
                } else if (OnWall(Wall.Right)) {
                    w = Wall.Right;
                } else if (OnWall(Wall.Floor)) {
                    w = Wall.Floor;
                } else if (OnWall(Wall.Ceiling)) {
                    w = Wall.Ceiling;
                } else if (OnWall(Wall.Back)) {
                    w = Wall.Back;
                }
                return w;
            }
        }
        public int intWall {
            get { return (int)wall; }
        }

        public int walls {
            get {
                return ((i==0 ? (int)Wall.Left : 0) |
                        (i==4 ? (int)Wall.Right : 0) | 
                        (j==0 ? (int)Wall.Floor : 0) |
                        (j==4 ? (int)Wall.Ceiling : 0) |
                        (k==0 ? (int)Wall.Back : 0));
            }       
        }

        public bool OnWall(Wall w) {
            return (walls & (int)w) == (int)w;
        }

        public Vector3 rotationToWall {
            get {
                // If on the back wall (or not on a wall), rotation is
                // identity (0, 0, 0).
                int[] rot = {0, 0, 0};
                switch(wall) {
                    case Wall.Right:
                        rot[1] = 90;
                        break;
                    case Wall.Left:
                        rot[1] = -90;
                        break;
                    case Wall.Ceiling:
                        rot[0] = -90;
                        break;
                    case Wall.Floor:
                        rot[0] = 90;
                        break;
                }
                return new Vector3(rot[0],rot[1],rot[2]);
            }
        }

        public Vector3 rotationOnWall {
            get {
                // Rotation on wall is determined by the l coordinate,
                // where l's value maps to degrees: (0 => 0, 1 => 90,
                // 2 => 180, 3 => 270). This rotation is with respect
                // to the right wall. Given a rotation on the right
                // wall, it maps to rotations on the other walls using
                // the formula explained below.
                // 
                // Rotation for back wall (wall==Wall.Back) and
                // interior positions (wall==Wall.None) is equivalent
                // to right wall rotation. If wall is Wall.None, then
                // use the back wall integer representation. Wall.Back
                // is equivalent to Wall.Right, since Wall.Back is 16
                // [log2(16)==4], and Wall.Right is 1 [log2(1)==0],
                // thus:
                // 
                // -90*log2(Wall.Back) == -360 == 0 == -90*log2(Wall.Right)
                // 
                int w = (wall==Wall.None ? (int)Wall.Back : intWall);

                // Translate coordinate l (right wall rotation) to the
                // rotation amount for the wall associated with this
                // position
                //
                // Given that right = 1, floor = 2, left = 4, ceiling = 8
                // 
                // rotation from right wall to floor is -90 and
                // log2(floor) = 1 --> (l * 90) - (90 * 1)
                //
                // rotation from right wall to left wall is -180 and
                // log2(left) = 2 --> (l * 90) - (90 * 2)
                //
                // rotation from right wall to ceiling is -270 and
                // log2(ceiling) = 3 --> (l * 90) - (90 * 3)
                //
                // so rotation = l*90 - log2(wall)*90 
                int rotationAmount = (l*90) - ((int)Mathf.Log(w,2)*90);

                int[] rot = {0, 0, 0};
                switch(wall) {
                    case Wall.Right:
                        rot[0] = rotationAmount;
                        break;
                    case Wall.Floor:
                        rot[1] = -rotationAmount;
                        break;
                    case Wall.Left:
                        rot[0] = -rotationAmount;
                        break;
                    case Wall.Ceiling:
                        rot[1] = rotationAmount;
                        break;
                    case Wall.Back:
                    case Wall.None:
                        rot[2] = -rotationAmount;
                        break;
                }
                return new Vector3(rot[0],rot[1],rot[2]);
            }
        }

        public Vector3 rotation {
            get {
                // final rotation = rotation to wall * rotation on wall
                return Quaternion.Euler(rotationOnWall) * rotationToWall;
            }
        }

        public Vector3 v3 {
            get{
                float[] v = {0f, 0f, 0f};
                switch (wall) {
                    case Wall.Back:
                        v[0] = (i-2) * 0.33f;
                        v[1] = (j-2) * 0.33f - 0.017f;
                        v[2] = 0.5f;
                        break;

                    case Wall.Left:
                        v[0] = -0.5f;
                        v[1] = (j-2) * 0.33f - 0.017f;
                        v[2] = -((k-2) * 0.33f);
                        break;

                    case Wall.Right:
                        v[0] = 0.5f;
                        v[1] = (j-2) * 0.33f - 0.017f;
                        v[2] = -((k-2) * 0.33f);
                        break;

                    case Wall.Floor:
                        v[0] = (i-2) * 0.33f;
                        v[1] = -0.5f;
                        v[2] = -((k-2) * 0.33f) - 0.017f;
                        break;

                    case Wall.Ceiling:
                        v[0] = (i-2) * 0.33f;
                        v[1] = 0.5f;
                        v[2] = -((k-2) * 0.33f) - 0.017f;
                        break;

                    default:
                        break;
                }
                return new Vector3(v[0],v[1],v[2]);
            }
        }

        int negmod(int v, int m) {
            if (v < 0) {
                v -= m * (Mathf.Abs(v)/m);
                v += m;
            }
            return v % m;
        }

        int negmod(float v, float m) {
            return negmod( (int) v, (int) m );
        }

        public Position ProjectToWall(int destL, int origL) {
            Position pos = new Position(this);
            destL = negmod(destL,4);
            int v = (int) Mathf.Pow(2, negmod(destL - origL,4));
            Wall destWall = (Wall) v;

            pos.l = origL;
            switch(destWall) {
                case Wall.Left:
                    pos.k = i;
                    pos.j = j;
                    pos.i = 0;
                    break;

                case Wall.Right:
                    pos.k = 4 - i;
                    pos.j = j;
                    pos.i = 4;
                    break;

                case Wall.Floor:
                    pos.i = i;
                    pos.k = j;
                    pos.j = 0;
                    break;

                case Wall.Ceiling:
                    pos.i = i;
                    pos.k = 4 - j;
                    pos.j = 4;
                    break;
            }

            return pos;
        }

        public Position transformToWall(Wall newWall) {

            int deltaWall = (int) negmod(( Mathf.Log((int) newWall,2) -
                                           Mathf.Log((int) wall,2) ), 4);
            Position proj = projection;
            Position pos = proj.ProjectToWall(proj.l + deltaWall, l);
            
            return pos;
        }

        public Position projection {
            get {
                Position pos = new Position(this);
                if (!OnWall(Wall.Back)) {
                    // project to back wall
                    //
                    // Given:
                    //  current wall position
                    //  i,j,k,l
                    // 
                    pos.k = 0;
                    switch(pos.wall) {
                        case Wall.Left:
                            pos.i = k;
                            pos.j = j;
                            break;

                        case Wall.Right:
                            pos.i = (4-k);
                            pos.j = j;
                            break;

                        case Wall.Floor:
                            pos.i = i;
                            pos.j = k;
                            break;

                        case Wall.Ceiling:
                            pos.i = i;
                            pos.j = (4-k);
                            break;
                    }
                    pos.l = ((int)Mathf.Log(intWall,2) + l) % 4;
                    // Debug.Log("this: "+this);
                    // Debug.Log("pos: "+pos);

                } else {
                    // Project from back wall
                    switch(pos.l) {
                        case 0:
                            break;
                        case 1:
                            break;
                        case 2:
                            break;
                        case 3:
                            break;
                    }
                }
                return pos;   
            }
        }

    }

}
