namespace GameController {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;

    public partial class Objects: MonoBehaviour {
        public WallObjects walls;
        public Cameras cameras;
        public Lights lights;

        // The shape on the side walls that the player controls
        public GameObject playerShape;
        // The shape projected from the player shape to the back wall
        public GameObject projectedShape;

        // The door panel with the shape lock
        public GameObject doorPanel;
    }
}
        
