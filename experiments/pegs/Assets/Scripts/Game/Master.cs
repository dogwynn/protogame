namespace GameController {
    using UnityEngine;
    using System;
    using System.Collections;
    using UnityEngine.UI;
    using UnityEngine.SceneManagement;
    using System.Collections.Generic;
    using GameMessages;

    public partial class Master : MessageHandler {
        public int verbosity=0;
        
        // The material for the room walls to be modified during the
        // collapse animation
        public Material wallMaterial;

        // Configurable time till the collapse animation begins
        public float timeTillCollapse;
        private float collapseTimeCheckStart;

        // When true, the user should be informed visually and otherwise
        // that the room/connection is about collapse
        public bool startCollapse = false;
        public bool isCollapsed = false;
        public bool collapseFinished = false;
        private float collapseStartTime; 
        // private float normalFrequency = 1.2f;
        private float normalAmplitude = 0.3f;
        private float bumpScale = 0f;

        public Text collapseTimerText;
        private float collapseTimer;

        public float normalFrequencyStart = 0.01f;
        public float normalFrequencyEnd = 0.5f;

        private float normalPhase = 0.0f;
        private float normalDelta = 0.0f;
    
        private float initialBumpScale = 0.0f;
        public Color initialEmission;

        private bool collapseCoroutineStarted=false;
        private Coroutine collapseCoroutine;
        // private Coroutine endOfGameCoroutine;

        private Message collapseStartMessage;
        private Message collapseCancelledMessage;
        private Message collapseFinishedMessage;
        private Message gameOverMessage;

        void Awake() {
            MessageBus.verbosity = verbosity;

            collapseStartMessage = MessageUtils.New(MessageType.CollapseStart);
            collapseCancelledMessage = MessageUtils.New(
                MessageType.CollapseCancelled
            );
            collapseFinishedMessage = MessageUtils.New(
                MessageType.CollapseFinished
            );

            gameOverMessage = MessageUtils.New(MessageType.GameOver);

            GetInitialWallValues();
        }

        void Start() {

            SetTimerText();
            RestartCollapseTimer();

        }

        void GetInitialWallValues() {
            // file:///Applications/Unity/Unity.app/Contents/Documentation/en/Manual/MaterialsAccessingViaScript.html
            wallMaterial.EnableKeyword("_EMISSION");
            wallMaterial.EnableKeyword("_NORMALMAP");

            wallMaterial.SetFloat("_BumpScale", initialBumpScale);
            wallMaterial.SetColor("_EmissionColor", initialEmission);
            // initialBumpScale = wallMaterial.GetFloat("_BumpScale");
            // initialEmission = wallMaterial.GetColor("_EmissionColor");
        }

        void SendGameOverMessage() {
            MessageBus.current.SendMessage(gameOverMessage);
        }

        void RestartCollapseTimer() {
            if (!isCollapsed) {
                normalDelta = 0.0f;
                bumpScale = 0.0f;
                normalAmplitude = 0.3f;
                collapseTimer = timeTillCollapse;
                if (startCollapse) {
                    // undo collapse animation
                    Debug.Log("Stopping collapse...");
                    SendCollapseCancelledMessage();
                    startCollapse = false;
                }

                collapseTimeCheckStart = Time.time;
                if (collapseCoroutine != null) {
                    StopCoroutine(collapseCoroutine);
                }
                if (collapseCoroutineStarted) {
                    collapseCoroutineStarted = false;
                }
                collapseCoroutine = StartCoroutine(StartCollapseTimer());
            } else {
                Debug.Log("Trying to restart timer after collapse. Ignoring..");
            }
        }

        void SetTimerText() {
            var roundedRestSeconds = Mathf.CeilToInt(collapseTimer);
            var displaySeconds = roundedRestSeconds % 60;
            var displayMinutes = roundedRestSeconds / 60;

            var text = String.Format(
                "{0:00}:{1:00}", displayMinutes, displaySeconds
            );
            collapseTimerText.text = text;
            
        }

        void Update() {
            collapseTimer -= Time.deltaTime;

            SetTimerText();

            UpdateWalls();
        }

        public override void HandleMessage(Message message) {
            switch(message.type) {
                case MessageType.NextShape:
                    RestartCollapseTimer();
                    break;
                case MessageType.GameOver:
                    EndGame();
                    break;
            }
            
        }

        void EndGame() {
            SceneManager.LoadScene(0);
        }

        void SendCollapseStartMessage() {
            Debug.Log("Starting collapse...");
            MessageBus.current.SendMessage(collapseStartMessage);
        }
        
        void SendCollapseCancelledMessage() {
            Debug.Log("Cancelling collapse...");
            MessageBus.current.SendMessage(collapseCancelledMessage);
        }
        
        void SendCollapseFinishedMessage() {
            Debug.Log("Finished collapse..."); 
            MessageBus.current.SendMessage(collapseFinishedMessage);
        }
        
        IEnumerator StartCollapseTimer() {
            while( (Time.time - collapseTimeCheckStart) < timeTillCollapse ) {
                yield return new WaitForSeconds(1f);
            }

            SendCollapseStartMessage();
            startCollapse = true;
            collapseStartTime = Time.time;

            float nsteps = 100f;
            for(int i=0; i<nsteps; i++) {
                if (!startCollapse) {
                    // XXX HACKHACKHACK
                    yield break;
                }
                yield return new WaitForSeconds(0.1f);
                // Debug.Log(normalDelta);
                normalDelta = i / nsteps;
                normalAmplitude += 0.02f;
            }
            if (!startCollapse) {
                // XXX HACKHACKHACK
                yield break;
            }
            yield return new WaitForSeconds(0.1f);
            normalDelta = 1.0f;
            isCollapsed = true;

        }

        IEnumerator StartEndOfGame() {
            PerspectiveToOrthographic persp2ortho = Objects.current.cameras.main
                .GetComponent<PerspectiveToOrthographic>();
            persp2ortho.ToOrtho(2f);

            while (!persp2ortho.transitionDone) {
                yield return new WaitForSeconds(0.1f);
            }

            SendGameOverMessage();
        }

        void UpdateWalls() {
            // Update the normal ("bump") map scale value
            if (startCollapse && !collapseFinished) {
                // Debug.Log(normalAmplitude);
                // Debug.Log(normalFrequency+" "+normalAmplitude);
                // Debug.Log(normalDelta);
                float t = Time.time - collapseStartTime;
                normalPhase = (
                    2 * Mathf.PI * t *
                    (normalFrequencyStart +
                     (normalFrequencyEnd - normalFrequencyStart) * normalDelta / 2)
                );

                bumpScale = ( normalAmplitude * Mathf.Sin(normalPhase) );
                if (isCollapsed) {
                    if (Mathf.Abs(bumpScale-normalAmplitude) < 1e-3) {
                        if (bumpScale<0) {
                            bumpScale = -normalAmplitude;
                        } else {
                            bumpScale = normalAmplitude;
                        }
                        collapseFinished = true;
                        SendCollapseFinishedMessage();
                        StartCoroutine(StartEndOfGame());
                        // endOfGameCoroutine = StartCoroutine(StartEndOfGame());
                    }
                }

                wallMaterial.SetFloat(
                    "_BumpScale",
                    bumpScale
                );
            } else {
                wallMaterial.SetFloat(
                    "_BumpScale",
                    bumpScale
                );
            }

            Color emissionColor = initialEmission;

            if (startCollapse) {
                emissionColor = Color.red;
            }

            // Update the emission color value
            Color newEmit = 0f * emissionColor;
            if (!collapseFinished) {
                float floor = 0.2f;
                float ceiling = 1.2f;
                float emission = floor + Mathf.PingPong( .8f * Time.time,
                                                         ceiling - floor );
                newEmit = emission * emissionColor;
            }
            wallMaterial.SetColor("_EmissionColor", newEmit);
        }

        void ResetWalls() {
            // wallMaterial.SetFloat("_BumpScale", initialBumpScale);
            // wallMaterial.SetColor("_EmissionColor", initialEmission);
            wallMaterial.SetFloat("_BumpScale", initialBumpScale);
            wallMaterial.SetColor("_EmissionColor", initialEmission);
        }

        void OnDestroy() {
            ResetWalls();
        }

    }
}
