namespace GameController {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using GameMessages;
    using GameUtilities;
    
    public partial class MatchLogic: MessageHandler {
        public bool positionMatched = false;
        public bool rotationMatched = false;

        private GameObject playerObject;
        private Position playerPosition;
        private int playerRotation;
        private GameObject doorObject;
        private Position doorPosition;
        private int doorRotation;

        private Message positionMatchedMessage;
        private Message rotationMatchedMessage;

        void Awake() {
            positionMatchedMessage = MessageUtils.New(
                MessageType.ShapePositionMatched
            );
            rotationMatchedMessage = MessageUtils.New(
                MessageType.ShapeRotationMatched
            );
            // MessageUtils.PrintSpec(positionMatchedMessage);
        }
        
        public override void HandleMessage(Message message) {
            bool doCheck = false;
            switch(message.type) {
                case MessageType.PlayerPosition:
                    playerObject = message.go0;
                    playerRotation = message.i0;
                    playerPosition = message.p0;
                    doCheck = true;
                    break;
                case MessageType.DoorPosition:
                    doorObject = message.go0;
                    doorRotation = message.i0;
                    doorPosition = message.p0;
                    doCheck = true;
                    break;
            }

            if (doorPosition != null && playerPosition != null) {
                if (doCheck) {
                    CheckPositionMatch();
                    CheckRotationMatch();
                }
            }
                
        }

        void CheckPositionMatch() {
            positionMatched = playerPosition.projection.MatchIJK(doorPosition);
            SendPositionMatchedMessage(positionMatched);
        }

        void CheckRotationMatch() {
            rotationMatched = playerPosition.projection.l == doorPosition.l;
            SendRotationMatchedMessage(rotationMatched);
        }

        void SendRotationMatchedMessage(bool matched) {
            rotationMatchedMessage.b0 = matched;

            rotationMatchedMessage.go0 = doorObject;
            rotationMatchedMessage.p0 = doorPosition;
            rotationMatchedMessage.i0 = doorRotation;

            rotationMatchedMessage.go1 = playerObject;
            rotationMatchedMessage.p1 = playerPosition;
            rotationMatchedMessage.i1 = playerRotation;

            MessageBus.current.SendMessage(rotationMatchedMessage);
        }

        void SendPositionMatchedMessage(bool matched) {
            positionMatchedMessage.b0 = matched;

            positionMatchedMessage.go0 = doorObject;
            positionMatchedMessage.p0 = doorPosition;
            positionMatchedMessage.i0 = doorRotation;

            positionMatchedMessage.go1 = playerObject;
            positionMatchedMessage.p1 = playerPosition;
            positionMatchedMessage.i1 = playerRotation;

            MessageBus.current.SendMessage(positionMatchedMessage);
        }
    }
}
