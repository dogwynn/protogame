﻿namespace GameController {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using GameMessages;
    using GameUtilities;

    public class LevelLogic : MessageHandler {
        private Message nextShapeMessage;
        private Message levelStartMessage;
        private Message levelCompleteMessage;
        private Message successfulShapeMatchMessage;
        private Message failedShapeMatchMessage;

        public bool shapeRotationMatched = false;
        public bool shapePositionMatched = false;

        void Awake() {
            nextShapeMessage = MessageUtils.New(
                MessageType.NextShape
            );
            levelStartMessage = MessageUtils.New(
                MessageType.LevelStart
            );
            levelCompleteMessage = MessageUtils.New(
                MessageType.LevelStart
            );

            successfulShapeMatchMessage = MessageUtils.New(
                MessageType.SuccessfulShapeMatch
            );
            failedShapeMatchMessage = MessageUtils.New(
                MessageType.FailedShapeMatch
            );
        }
        
        public override void HandleMessage(Message message) {
            switch(message.type) {
                case MessageType.GameStart:
                    break;
                case MessageType.ShapeRotationMatched:
                    shapeRotationMatched = message.b0;
                    break;
                case MessageType.ShapePositionMatched:
                    shapePositionMatched = message.b0;
                    break;
                case MessageType.TestShapeMatch:
                    if (shapeRotationMatched && shapePositionMatched) {
                        SendSuccessfulShapeMatchMessage();
                        SendNextShapeMessage();
                    } else {
                        SendFailedShapeMatchMessage();
                    }
                    break;
            }
            
        }

        void Start() {
            SendLevelStartMessage();
        }

        private void SendNextShapeMessage() {
            nextShapeMessage.i0 = Random.Range(0,3);
            MessageBus.current.SendMessage(nextShapeMessage);
        }

        private void SendLevelStartMessage() {
            levelStartMessage.i0 = Random.Range(0,3);
            MessageBus.current.SendMessage(levelStartMessage);
        }

        private void SendLevelCompleteMessage() {
            MessageBus.current.SendMessage(levelCompleteMessage);
        }

        private void SendSuccessfulShapeMatchMessage() {
            Debug.Log("Shape matched!");
            MessageBus.current.SendMessage(successfulShapeMatchMessage);
        }

        private void SendFailedShapeMatchMessage() {
            Debug.Log("FAILED shape match!!");
            MessageBus.current.SendMessage(failedShapeMatchMessage);
        }

    }
}
