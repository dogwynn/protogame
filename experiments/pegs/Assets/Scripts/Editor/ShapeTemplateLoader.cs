namespace Editor {
    using System.Collections;
    using System.Collections.Generic;
    using System.IO;
    using System.Text.RegularExpressions;

    using UnityEditor;
    using UnityEngine;

    using SimpleJSON;
    using GameUtilities;

    public class ShapeTemplate {
        public ShapeTemplate() {
        }
    }

    public class ShapeTemplateLoader {
        private static string databaseDir = Path.Combine(
            "Assets","ShapeDatabase"
        );

        public ShapeTemplateLoader() {
        }

        public IEnumerable<ShapeTemplate> GetTemplates() {
            Debug.Log(databaseDir);
            foreach (FileInfo f in Utils.GetFiles(databaseDir,"*.json")) {
                Debug.Log(f.FullName);
                using(StreamReader s = f.OpenText()) {
                    var N = JSON.Parse(s.ReadToEnd());
                    Debug.Log("N:");
                    Debug.Log(N);
                }
                yield return new ShapeTemplate();
            }
        }
    }
}
