namespace Editor {
    using UnityEditor;
    using UnityEngine;
    using System.Text.RegularExpressions;

    public class ShapeBuilder : EditorWindow {
        private string shapeName = "";
        private Regex shapeNumberRegex = new Regex(@"(\d+)$");
        private int shapeNumber = 0;
        private bool groupEnabled;
        private bool myBool = true;
        private float myFloat = 1.23f;

        private Texture shapeTexture;

        private ShapeTemplateLoader loader = new ShapeTemplateLoader();

        // Add menu item named "My Window" to the Window menu
        [MenuItem("Window/Shape Builder")]
        public static void ShowWindow() {
            //Show existing window instance. If one doesn't exist, make one.
            EditorWindow.GetWindow(typeof(ShapeBuilder));
        }


        void OnEnable() {
            // foreach(ShapeTemplate st in loader.GetTemplates()) {
            //     Debug.Log(st);

            // }
        }
    
        void OnGUI() {
            EditorGUILayout.BeginVertical();
            // Dropdown of existing shapes?  Selecting a shape here loads
            //   its settings so you can tweak its settings and create
            //   new. Need to persist shape definitions, which is a
            //   downside.
            //
            // Set shape texture
            shapeTexture = (Texture)EditorGUILayout.ObjectField(
                "Texture", shapeTexture, typeof(Texture), false
            );

            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Set Name/Number from Texture")) {
                string name = shapeTexture.name;
                Match match = shapeNumberRegex.Match(name);
                bool found = false;
                if (match.Success) {
                    string number = match.Groups[1].Value;
                    if (System.Int32.TryParse(number, out shapeNumber)) {
                        name = shapeNumberRegex.Replace(name, "");
                        found = true;
                    } 
                }
                if (!found) {
                    shapeNumber = 0;
                }
                shapeName = name;
            }
            EditorGUILayout.EndHorizontal();

            // Set name of shape
            shapeName = EditorGUILayout.TextField ("Name", shapeName);
            // Set number/id of shape
            shapeNumber = EditorGUILayout.IntField ("Number", shapeNumber);

            GUILayout.Label("Base Settings", EditorStyles.boldLabel);

        
            groupEnabled = EditorGUILayout.BeginToggleGroup ("Optional Settings", groupEnabled);
            myBool = EditorGUILayout.Toggle ("Toggle", myBool);
            myFloat = EditorGUILayout.Slider ("Slider", myFloat, -3, 3);
            EditorGUILayout.EndToggleGroup ();
            EditorGUILayout.EndVertical();
        }
    }
}
