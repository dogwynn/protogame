namespace RoomPosition {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using GameController;
    using GameUtilities;
    using GameMessages;

    public interface IRoomPositioner {
        int i { get; set; }
        int j { get; set; }
        int k { get; set; }
        int l { get; set; }
        Position position { get; set; }
        int ClampI(int value);
        int ClampJ(int value);
        int ClampK(int value);
        int ClampL(int value);
        Vector3 roomVector { get; }
    }

    public interface IRandomPosition {
        void ResetRandomPosition();
    }

    [System.Serializable]
    public class WallDistances {
        public float leftWallDistance = 0.01f;
        public float backWallDistance = 0.01f;
        public float rightWallDistance = 0.01f;
        public float ceilingDistance = 0.01f;
        public float floorDistance = 0.01f;
    }

    public class RoomPositionerMonoBehaviour : MessageHandler, IRoomPositioner {
        // Converts Position class's [i, j, k] room coordinates to [x,
        // y, z] world positions
        //
        // Should be subclassed by GameObjects. Methods to override:
        //
        // * ClampI, ClampJ, ClampK, ClampL
        //
        // 
        public WallDistances wallDist; 

        private Position _position = new Position();
        private Vector3 _roomVector;

        public override void HandleMessage(Message message) {}

        public Transform backWall {
            get { return Objects.current.walls.back.transform; }
        }

        public Transform rightWall {
            get { return Objects.current.walls.right.transform; }
        }

        public Transform leftWall {
            get { return Objects.current.walls.left.transform; }
        }

        public Transform ceiling {
            get { return Objects.current.walls.ceiling.transform; }
        }

        public Transform floor {
            get { return Objects.current.walls.floor.transform; }
        }

        // Clamping functions
        // 
        // By default, the Position object clamps i, j and k to be
        // between [0 .. 4] and l to be between [0 .. 3]. Subclasses
        // are meant to override these (if necessary) to clamp to
        // something other than the default.
        // 
        // In general, the purpose of clamping is to constrain the
        // movement of a GameObject to be on some subset of room
        // walls. I.e. if a subclass does not override at least one of
        // these, then it will be allowed to freely float within the
        // room space as well as on the walls.
        // 
        public virtual int ClampI(int value) { return value; }
        public virtual int ClampJ(int value) { return value; }
        public virtual int ClampK(int value) { return value; }
        public virtual int ClampL(int value) { return value; }

        public int i {
            get { return _position.i; }
            set {
                position = new Position(
                    value, _position.j,  _position.k, _position.l
                );
            }
        }

        public int j {
            get { return _position.j; }
            set {
                position = new Position(
                    _position.i, value,  _position.k, _position.l
                );
            }
        }

        public int k {
            get { return _position.k; }
            set {
                position = new Position(
                    _position.i, _position.j, value, _position.l
                );
            }
        }

        public int l {
            get { return _position.l; }
            set {
                position = new Position(
                    _position.i, _position.j, _position.k, value
                );
            }
        }

        public Position position {
            get {
                return new Position(_position);
            }

            set {
                Position pos = new Position(
                    ClampI(value.i), ClampJ(value.j), ClampK(value.k), value.l
                );
                if ( pos != _position ) {
                    _position = pos;
                    _ResetRoomPosition();
                }
            }
        }

        public Wall wall {
            get { return _position.wall; }
        }

        public bool OnWall( Wall w ) {
            return _position.OnWall(w);
        }

        protected virtual void _ResetRoomPosition() {
            Vector3 v = _position.v3;
            float x = v.x;
            float y = v.y;
            float z = v.z;

            switch (wall) {
                case Wall.Back:
                    z -= wallDist.backWallDistance;
                    break;

                case Wall.Left:
                    x += wallDist.leftWallDistance;
                    break;

                case Wall.Right:
                    x -= wallDist.rightWallDistance;
                    break;

                case Wall.Floor:
                    y += wallDist.floorDistance;
                    break;

                case Wall.Ceiling:
                    y -= wallDist.ceilingDistance;
                    break;
            }
        
            _roomVector = new Vector3(x,y,z);
        }

        public Vector3 roomVector {
            get { return _roomVector; }
        }
        
        public virtual void ResetRoomPosition() {
            transform.eulerAngles = _position.rotationToWall;
            transform.Rotate(
                _position.rotationOnWall,
                Space.World
            );
            transform.localPosition = roomVector;
        }

    }

}
