﻿using UnityEngine;
using System.Collections;

// Cribbed from:
//
// http://forum.unity3d.com/threads/smooth-transition-between-perspective-and-orthographic-modes.32765/
//
// 
public class PerspectiveToOrthographic : MonoBehaviour {
    private Matrix4x4   ortho,
                        perspective;
    public float orthographicSize = 5f;
    private float       aspect;
    private bool        orthoOn;
    private Camera cam;

    public bool transitionDone = false;
    
    public static Matrix4x4 MatrixLerp(Matrix4x4 src, Matrix4x4 dest, float time)
    {
        Matrix4x4 ret = new Matrix4x4();
        for (int i = 0; i < 16; i++)
            ret[i] = Mathf.Lerp(src[i], dest[i], time);
        return ret;
    }
 
    private IEnumerator LerpFromTo(Matrix4x4 src, Matrix4x4 dest, float duration)
    {
        float startTime = Time.time;
        while (Time.time - startTime < duration)
        {
            cam.projectionMatrix = MatrixLerp(src, dest, (Time.time - startTime) / duration);
            yield return 1;
        }
        cam.projectionMatrix = dest;
        transitionDone = true;
    }
 
    public Coroutine BlendToMatrix(Matrix4x4 targetMatrix, float duration)
    {
        StopAllCoroutines();
        transitionDone = false;
        return StartCoroutine(LerpFromTo(cam.projectionMatrix, targetMatrix, duration));
    }

    void Start() { 
        cam = GetComponent<Camera>();
        aspect = (float) Screen.width / (float) Screen.height;
        ortho = Matrix4x4.Ortho(
            -orthographicSize * aspect,
            orthographicSize * aspect,
            -orthographicSize,
            orthographicSize,
            cam.nearClipPlane, cam.farClipPlane
        );
        perspective = Matrix4x4.Perspective(
            cam.fieldOfView, aspect,
            cam.nearClipPlane, cam.farClipPlane
        );
        // cam.projectionMatrix = ortho;
        orthoOn = false;
    }

    public void ToOrtho(float duration) {
        if (!orthoOn) {
            orthoOn = true;
            BlendToMatrix(ortho, duration);
        }
    }

    public void ToPerspective(float duration) {
        if (orthoOn) {
            orthoOn = false;
            BlendToMatrix(perspective, duration);
        }
    }
}
