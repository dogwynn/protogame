namespace MathExtensions {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;

    public static class Vector3Methods {
        public static Vector3 RoundToInt(this Vector3 v) {
            return new Vector3(
                Mathf.RoundToInt(v.x),
                Mathf.RoundToInt(v.y),
                Mathf.RoundToInt(v.z)
            );
        }
    }

}
