namespace Shapes {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using GameController;
    using GameMessages;
    using MathExtensions;

    public class ShapeSelector : MonoBehaviour, INextShape {
        public List<GameObject> shapes = new List<GameObject>();
        public GameObject shape;
        public GameObject testShape;

        public static bool useTestShape = false;

        private GameObject currentShape;

        void Awake() {
            // Destroy(testShape);
            testShape.SetActive(false);
        }

        public void NextShape(int shapeIndex) {
            SelectShape(shapeIndex);
        }
        
        public void SelectShape(int shapeIndex) {
            // For some reason, even if an object is Destroyed, its
            // Renderer will still show up in GetComponentsInChildren
            // unless you deactivate it first.
            if (currentShape != null) {
                currentShape.SetActive(false);
            }
            Destroy(currentShape);

            currentShape = (GameObject) Instantiate(
                shapes[shapeIndex]
            );

            currentShape.transform.SetParent(shape.transform,false);

            // Set the alpha for the new shape to 0
            Renderer renderer = currentShape.GetComponent<Renderer>();
            Color newColor = renderer.material.color;
            newColor.a = 0.0f;
            renderer.material.SetColor("_Color", newColor) ;

            // Debug.Log("ShapeSelector: " + currentShape);

            // Renderer[] robs = GetComponentsInChildren<Renderer>();
            // foreach(Renderer r in robs) {
            //     Debug.Log("SS: "+gameObject+" renderer: "+r);
            // }
        }

    }
}
