namespace Door {
    using UnityEngine;
    using System.Collections;
    using RoomPosition;
    using GameUtilities;
    using GameMessages;
    using Shapes;

    public partial class Positioner : RoomPositionerMonoBehaviour, IRandomPosition, INextShape {
        private Message positionMessage;
        public int currentRotation;

        void Awake() {
            positionMessage = MessageUtils.New(MessageType.DoorPosition);
        }

        // Preclude Right and Left walls
        public override int ClampI(int value) {
            return value > 3 ? 3 : (value < 1 ? 1 : value);
        }
        // Preclude Ceiling and Floor
        public override int ClampJ(int value) {
            return value > 3 ? 3 : (value < 1 ? 1 : value);
        }
        // Clamp to Back wall;
        public override int ClampK(int value) {
            return 0;
        }

        void SendPositionMessage() {
            positionMessage.p0 = position;
            positionMessage.go0 = gameObject;
            positionMessage.i0 = currentRotation;
            MessageBus.current.SendMessage(positionMessage);
        }

        public void NextShape(int shapeIndex) {
            ResetRandomPosition();
        }

        public void ResetRandomPosition() {
            // Select random position on that wall
            position = Position.RandomPosition(Wall.Back);

            ResetRoomPosition();

            SendPositionMessage();
        }

    }
}
