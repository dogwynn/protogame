namespace Door {
    using UnityEngine;
    using System.Collections;
    using GameMessages;

    public class LevelStartMessageHandler: MessageHandler {
        public override void HandleMessage(Message message) {
            Positioner.current.ResetRandomPosition();
        }
    }
}
