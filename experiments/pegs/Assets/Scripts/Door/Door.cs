﻿namespace Door {
    using UnityEngine;
    using System.Collections;
    using RoomPosition;
    using GameController;

    public static class Names {
        public static string objectName = "Door Panel";
    }

    public partial class Positioner : RoomPositionerMonoBehaviour {
        private static Positioner _current;
        public static Positioner current {
            get {
                if (!_current) {
                    _current = FindObjectOfType( typeof(Positioner) )
                        as Positioner;
                }
                return _current;
            }
        }
    }

    public partial class Matcher : MonoBehaviour {
        private static Matcher _current;
        public static Matcher current {
            get {
                if (!_current) {
                    _current = FindObjectOfType( typeof(Matcher) )
                        as Matcher;
                }
                return _current;
            }
        }
    }
}
