namespace Door {
    using UnityEngine;
    using System.Collections;
    using RoomPosition;
    using GameUtilities;
    using GameMessages;
    using Shapes;

    public partial class ShapeChangeHandler : MessageHandler {
        public float fadeOutTime = 0.1f;
        public float fadeInTime = 0.3f;
        
        private FadeObjectInOut fader;
        // private Positioner positioner;
        // private IRandomPosition positioner;
        // private ShapeSelector selector;

        void Awake() {
            fader = GetComponent<FadeObjectInOut>();
            // positioner = GetComponent<IRandomPosition>();
            // selector = GetComponent<ShapeSelector>();
        }

        public override void HandleMessage(Message message) {
            switch(message.type) {
                case MessageType.LevelStart:
                    Debug.Log("ShapeChangeHandler: LevelStart");
                    StartCoroutine(GotoNextShape(message.i0));
                    break;
                case MessageType.NextShape:
                    Debug.Log("ShapeChangeHandler: NextShape");
                    StartCoroutine(GotoNextShape(message.i0));
                    break;
            }
            
        }

        IEnumerator GotoNextShape(int shapeIndex) {
            Debug.Log("Goto index: "+shapeIndex);
            fader.FadeOut(fadeOutTime);
            while(!fader.fadeDone) {
                yield return new WaitForSeconds(0.01f);
            }

            INextShape[] nextShapeComps = GetComponentsInChildren<INextShape>();
            foreach (INextShape comp in nextShapeComps) {
                comp.NextShape(shapeIndex);
            }

            fader.FadeIn(fadeInTime);
            while(!fader.fadeDone) {
                yield return new WaitForSeconds(0.01f);
            }
        }

    }
}
