namespace ProjectedShape {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using RoomPosition;
    using GameUtilities;
    using GameMessages;

    public partial class Positioner :
        RoomPositionerMonoBehaviour, IMessageHandler {

        public override void HandleMessage(Message message) {
            if (message.type == MessageType.PlayerPosition) {
                UpdatePosition(message.go0, message.p0);
            } 
        }

        public void UpdatePosition(GameObject playerObj, Position playerPos) {
            // From the player position, get the position and rotation
            // on the back wall
            k = 0;

            position = playerPos.projection;
            
            ResetRoomPosition();
        }
    }
}
