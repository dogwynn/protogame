﻿namespace ProjectedShape {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using RoomPosition;
    using GameMessages;

    public partial class Positioner :
        RoomPositionerMonoBehaviour, IMessageHandler {
        private static Positioner _current;
        public static Positioner current {
            get {
                if (!_current) {
                    _current = FindObjectOfType( typeof(Positioner) )
                        as Positioner;
                }
                return _current;
            }
        }
    }

}
