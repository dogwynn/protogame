namespace ProjectedShape {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using RoomPosition;
    using GameUtilities;
    using GameMessages;

    public partial class MatchHandler : MonoBehaviour, IMessageHandler {

        public void HandleMessage(Message message) {
            if (message.type == MessageType.ShapePositionMatched) {
                // Debug.Log("ProjectedShape.MatchHandler: position matched");
            } else if (message.type == MessageType.ShapeRotationMatched) {
                // Debug.Log("ProjectedShape.MatchHandler: rotation matched");
            } 
        }

    }
}
