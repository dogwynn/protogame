namespace GameMessages {
    using UnityEngine;
    using System.Collections;

    public interface IMessageHandler {
        void HandleMessage( Message message );
    }

    public abstract class MessageHandler : MonoBehaviour, IMessageHandler {
        public abstract void HandleMessage( Message message );
    }
}
