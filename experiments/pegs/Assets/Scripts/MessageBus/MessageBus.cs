namespace GameMessages {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;

    public class MessageBus {
        public static int verbosity;

        Dictionary<MessageType, List<MessageSubscriber>> subscriberLists = 
            new Dictionary<MessageType, List<MessageSubscriber>>();

        public void RemoveSubscriber(MessageSubscriber subscriber) {
            MessageType[] messageTypes = subscriber.messageTypes;
            if(verbosity > 4) {
                Debug.Log("Removing subscriber: "+subscriber.handler);
            }
            foreach(MessageType type in messageTypes) {
                subscriberLists[type].Remove(subscriber);
            }
        }

        public void AddSubscriber( MessageSubscriber subscriber ) {
            MessageType[] messageTypes = subscriber.messageTypes;
            for (int i = 0; i < messageTypes.Length; i++) {
                AddSubscriberToMessage (messageTypes[i], subscriber);
            }
        }

        void AddSubscriberToMessage ( MessageType messageType, 
                                      MessageSubscriber subscriber) {
            if (!subscriberLists.ContainsKey (messageType)) {
                if (verbosity > 4) {
                    Debug.Log("Creating new key for  "+messageType);
                }
                subscriberLists [messageType] = 
                    new List<MessageSubscriber> ();
            }

            if (verbosity > 4) {
                Debug.Log("New subscriber for message type ("+
                          messageType+"): "+subscriber.handler);
            }
            subscriberLists [messageType].Add (subscriber);
        }

        public void SendMessage (Message message) {
            if (!subscriberLists.ContainsKey (message.type)) {
                if (verbosity > 4) {
                    Debug.Log("No subscribers for "+message.type);
                }
                return;
            }

            List<MessageSubscriber> subscriberList = 
                subscriberLists [message.type];

            for (int i = 0; i < subscriberList.Count; i++) {
                SendMessageToSubscriber (message, subscriberList[i]);
            }

        }

        void SendMessageToSubscriber(Message message, 
                                     MessageSubscriber subscriber) {
            if (verbosity > 4) {
                Debug.Log("SEND<" + message.type + ">: " +
                          subscriber.handler);
            }
            subscriber.handler.HandleMessage(message);
        }

        /* Singleton */
        static MessageBus _current;

        public static MessageBus current {
            get {
                if(_current == null) {
                    if (verbosity > 0) {
                        Debug.Log("Creating instance of MessageBus");
                    }
                    _current = new MessageBus();
                }

                return _current;
            }
        }

        private MessageBus() {}
    }
}
