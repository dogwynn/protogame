namespace GameMessages {
    using System.Collections.Generic;
    using UnityEngine;
    using GameUtilities;

    public enum MessageType {
        NONE,
        Trace,
        GameStart,
        GameOver,
        LevelStart,
        LevelComplete,
        PlayerPosition,
        DoorPosition,
        CollapseStart,
        CollapseCancelled,
        CollapseFinished,
        ShapePositionMatched,
        ShapeRotationMatched,
        NextShape,
        PointAdded,
        EnterHoverOverWall,
        LeaveHoverOverWall,
        ClickWall,
        TestShapeMatch,
        SuccessfulShapeMatch,
        FailedShapeMatch,
        PlayerWADS
    }

    public struct Message {
        public MessageType type;
        public int i0;
        public int i1;
        public int i2;
        public float f0;
        public float f1;
        public float f2;
        public bool b0;
        public bool b1;
        public bool b2;
        public Position p0;
        public Position p1;
        public Position p2;
        public Vector3 v0;
        public Vector3 v1;
        public Vector3 v2;
        public GameObject go0;
        public GameObject go1;
        public GameObject go2;
        public Wall w0;
        public Wall w1;
        public Wall w2;
    }

    public static class MessageUtils {
        public static readonly Dictionary<MessageType, Dictionary<string,string>> specs =
            new Dictionary<MessageType, Dictionary<string,string>> {

            { MessageType.PlayerPosition,
              new Dictionary<string, string> {
                    {"p0", "player position"},
                    {"go0", "player game object"},
                    {"i0", "player rotation"}
                }
            },

            { MessageType.DoorPosition,
              new Dictionary<string, string> {
                    {"p0", "door position"},
                    {"go0", "door game object"},
                    {"i0", "door rotation"}
                }
            },

            { MessageType.ShapePositionMatched,
              new Dictionary<string, string> {
                    {"b0", "is it a match?"},
                    {"p0", "door position"},
                    {"go0", "door game object"},
                    {"i0", "door rotation"},
                    {"p1", "player position"},
                    {"go1", "player game object"},
                    {"i1", "player rotation"}
                }
            },

            { MessageType.ShapeRotationMatched,
              new Dictionary<string, string> {
                    {"b0", "is it a match?"},
                    {"p0", "door position"},
                    {"go0", "door game object"},
                    {"i0", "door rotation"},
                    {"p1", "player position"},
                    {"go1", "player game object"},
                    {"i1", "player rotation"}
                }
            },

            { MessageType.EnterHoverOverWall,
              new Dictionary<string, string> {
                    {"go0", "wall game object"},
                    {"w0", "wall entered"},
                }
            },

            { MessageType.LeaveHoverOverWall,
              new Dictionary<string, string> {
                    {"go0", "wall game object"},
                    {"w0", "wall left"},
                }
            }
        };

        public static bool HasField(Message m, string field) {
            return specs[m.type].ContainsKey(field);
        }

        public static void PrintSpec(Message m) {
            string s = "";
            if (specs.ContainsKey(m.type)) {
                foreach(string field in specs[m.type].Keys) {
                    s = s + field + ": " + specs[m.type][field] + "\n";
                }
            } else {
                s = "No spec for "+m.type;
            }
            Debug.Log(s);
        }

        public static Message New(MessageType type) {
            Message m = new Message();
            m.type = type;
            return m;
        }

    }
}
