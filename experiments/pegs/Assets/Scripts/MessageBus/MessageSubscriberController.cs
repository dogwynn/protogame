namespace GameMessages {
    using UnityEngine;
    using System.Collections;

    public class MessageSubscriberController : MonoBehaviour {
        public MessageType[] messageTypes;
        public MonoBehaviour handler;

        void Awake() {
            IMessageHandler imHandler = handler as IMessageHandler;
            if (imHandler != null) {
                MessageSubscriber subscriber = new MessageSubscriber ();
                subscriber.messageTypes = messageTypes;
                subscriber.handler = imHandler;
                MessageBus.current.AddSubscriber(subscriber);
            }
        }

        void OnDestroy() {
            IMessageHandler imHandler = handler as IMessageHandler;
            MessageSubscriber subscriber = new MessageSubscriber ();
            subscriber.messageTypes = messageTypes;
            subscriber.handler = imHandler;
            MessageBus.current.RemoveSubscriber(subscriber);
        }
    }
}
