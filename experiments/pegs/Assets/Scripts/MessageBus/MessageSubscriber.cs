namespace GameMessages {
    public struct MessageSubscriber {
        public MessageType[] messageTypes;
        public IMessageHandler handler;
    }
}
