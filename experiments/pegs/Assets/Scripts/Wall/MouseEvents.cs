namespace Wall {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using RoomPosition;
    using GameUtilities;
    using GameMessages;

    public class MouseEvents : MessageHandler {
        public Wall wall;

        private Message enterHoverOverWallMessage;
        private Message leaveHoverOverWallMessage;
        private Message clickWallMessage;

        void Awake() {
            enterHoverOverWallMessage = MessageUtils.New(
                MessageType.EnterHoverOverWall
            );

            leaveHoverOverWallMessage = MessageUtils.New(
                MessageType.LeaveHoverOverWall
            );

            clickWallMessage = MessageUtils.New(
                MessageType.ClickWall
            );
        }

        void SendEnterHoverOverWallMessage() {
            enterHoverOverWallMessage.go0 = gameObject;
            enterHoverOverWallMessage.w0 = wall;
            
            MessageBus.current.SendMessage(enterHoverOverWallMessage);
        }

        void SendLeaveHoverOverWallMessage() {
            leaveHoverOverWallMessage.go0 = gameObject;
            leaveHoverOverWallMessage.w0 = wall;
            
            MessageBus.current.SendMessage(leaveHoverOverWallMessage);
        }

        void SendClickWallMessage() {
            clickWallMessage.go0 = gameObject;
            clickWallMessage.w0 = wall;
            
            MessageBus.current.SendMessage(clickWallMessage);
        }

        void OnMouseEnter() {
            if (wall != Wall.None) {
                SendEnterHoverOverWallMessage();
            }
        }

        void OnMouseExit() {
            if (wall != Wall.None) {
                SendLeaveHoverOverWallMessage();
            }
        }

        void OnMouseUpAsButton() {
            if (wall != Wall.None) {
                SendClickWallMessage();
            }
        }


        public override void HandleMessage(Message message) {
        }

    }

}
