namespace Wall {
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using RoomPosition;
    using GameUtilities;
    using GameMessages;

    public class InteractionVisuals : MessageHandler {
        public Wall wall;

        public bool selected;
        public bool highlighted;

        void Select() {
            gameObject.layer = (int)Layers.SelectedWall;
            selected = true;
        }

        void Deselect() {
            gameObject.layer = (int)Layers.Wall;
            selected = false;
        }
        
        void Highlight() {
            if (!selected) {
                gameObject.layer = (int)Layers.HighlightedWall;
            }
        }

        void Unhighlight() {
            if (!selected) {
                gameObject.layer = (int)Layers.Wall;
            }
        }

        public override void HandleMessage(Message message) {
            if (message.type == MessageType.EnterHoverOverWall) {
                if (message.w0 == wall) {
                    Highlight();
                }
            } else if (message.type == MessageType.LeaveHoverOverWall) {
                if (message.w0 == wall) {
                    Unhighlight();
                }
            } else if (message.type == MessageType.ClickWall) {
                if (message.w0 == wall) {
                    Select();
                } else {
                    Deselect();
                }
            } else if (message.type == MessageType.PlayerPosition) {
                Position pos = message.p0;
                if (pos.wall == wall) {
                    Select();
                } else {
                    Deselect();
                }
            }
        }
    }
}
