# protogame

Repo for sketching design ideas, tech experiments, prototyping,
etc. for this game development experiment.

# Current goal

Develop a proof-of-concept web-based social game in three
months. **Doesn't need to be pretty or work very well.** Just needs to
be a complete expression of a game.

I.e. has:

* gameplay
* art
* some semblance of a backend design

#### Tech potentials

* HTML5
* JavaScipt 
* Node.js
* Python
* PHP

### Developers

* Robert O'Gwynn (rogwynn@gmail.com)
* David O'Gwynn (dogwynn@gmail.com)

### Artists

* Ian Hansen (?)
* Leslie ? (?)

### Collaborators

* Jon Micah Tyson (?)

